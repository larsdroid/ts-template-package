import { forty } from './module.js'

function f(): number {
    return forty() + 2
}

export { f }
