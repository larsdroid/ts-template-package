# ts-template-package

A template to be used as a **TypeScript NPM package**.  
Can be used to create a package that is used as a dependency (or devDependency)
of another JavaScript or TypeScript package or application. In other words, this can be used
to implement your own Node module in TypeScript.

## Included in this template

* NPM configuration (basically, the contents of our old trusty `package.json`)
* TypeScript configuration (configured in `tsconfig.json`)
* Vitest setup (tests are executed using `npm test`)
